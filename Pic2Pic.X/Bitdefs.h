//  Author: avishorp@gmail.com
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#ifndef __BITDEFS__
#define __BITDEFS__


#define BIT0HI   0b00000001
#define BIT1HI   0b00000010
#define BIT2HI   0b00000100
#define BIT3HI   0b00001000
#define BIT4HI   0b00010000
#define BIT5HI   0b00100000
#define BIT6HI   0b01000000
#define BIT7HI   0b10000000

#define BIT0LO   0b11111110
#define BIT1LO   0b11111101
#define BIT2LO   0b11111011
#define BIT3LO   0b11110111
#define BIT4LO   0b11101111
#define BIT5LO   0b11011111
#define BIT6LO   0b10111111
#define BIT7LO   0b01111111


#endif // __BITDEFS__