/*
 * File:   config.c
 * Author: jeremydahl
 *
 * Created on September 4, 2019, 1:52 PM
 */

// PIC16F15356 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1
#pragma config FEXTOSC = OFF    // External Oscillator mode selection bits (Oscillator not enabled)
#pragma config RSTOSC = HFINT32 // Power-up default value for COSC bits (HFINTOSC with OSCFRQ= 32 MHz and CDIV = 1:1)
#pragma config CLKOUTEN = OFF   // Clock Out Enable bit (CLKOUT function is disabled; i/o or oscillator function on OSC2)
#pragma config CSWEN = OFF      // Clock Switch Enable bit (The NOSC and NDIV bits cannot be changed by user software)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enable bit (FSCM timer enabled)

// CONFIG2
#pragma config MCLRE = ON       // Master Clear Enable bit (MCLR pin is Master Clear function)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config LPBOREN = OFF    // Low-Power BOR enable bit (ULPBOR disabled)
#pragma config BOREN = ON       // Brown-out reset enable bits (Brown-out Reset Enabled, SBOREN bit is ignored)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (VBOR) set to 1.9V on LF, and 2.45V on F Devices)
#pragma config ZCD = OFF        // Zero-cross detect disable (Zero-cross detect circuit is disabled at POR.)
#pragma config PPS1WAY = OFF    // Peripheral Pin Select one-way control (The PPSLOCK bit can be set and cleared repeatedly by software)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable bit (Stack Overflow or Underflow will cause a reset)

// CONFIG3
#pragma config WDTCPS = WDTCPS_31// WDT Period Select bits (Divider ratio 1:65536; software control of WDTPS)
#pragma config WDTE = OFF       // WDT operating mode (WDT Disabled, SWDTEN is ignored)
#pragma config WDTCWS = WDTCWS_7// WDT Window Select bits (window always open (100%); software control; keyed access not required)
#pragma config WDTCCS = SC      // WDT input clock selector (Software Control)

// CONFIG4
#pragma config BBSIZE = BB512   // Boot Block Size Selection bits (512 words boot block size)
#pragma config BBEN = OFF       // Boot Block Enable bit (Boot Block disabled)
#pragma config SAFEN = OFF      // SAF Enable bit (SAF disabled)
#pragma config WRTAPP = OFF     // Application Block Write Protection bit (Application Block not write protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block not write protected)
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration Register not write protected)
#pragma config WRTSAF = OFF     // Storage Area Flash Write Protection bit (SAF not write protected)
#pragma config LVP = ON         // Low Voltage Programming Enable bit (Low Voltage programming enabled. MCLR/Vpp pin function is MCLR.)

// CONFIG5
#pragma config CP = OFF         // UserNVM Program memory code protection bit (UserNVM code protection disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <stdint.h>
//#include <pic16f15375.h>

// Functions
void InitInputPorts(){
    // Port A
    LATA =      0x00;   // start low
    TRISA =     0xFF;   // set port A as input
    ANSELA =    0x00;   // set port A to digital I/O
    WPUA =      0xFF;   // pull ups enabled

    // Port B
    LATB =      0x00;   // start low
    TRISB =     0xFF;   // set port B as input
    ANSELB =    0x00;   // set port B to digital I/O
    WPUB =      0xFF;   // pull ups enabled
    
    // Port D
    LATD =      0x00;   // start low
    TRISD =     0xFF;   // set port D as input
    ANSELD =    0x00;   // set port D to digital I/O
    WPUD =      0xFF;   // pull ups enabled
    
//    LATB =      0x00;   // start low
//    TRISB =     0x00;   // set port B as output
//    ANSELB =    0x00;   // set port B to digital I/O
    //    LATB =      0x55;   // b0101010101 initial state
}

void InitEUSART(){
// EUSART Transmission Initialization
    // Steps are found in p471
    // Initialized at 9600 baud
    TRISCbits.TRISC6 = 0;
    TRISCbits.TRISC7 = 1;
    
    // Disable Analog on RC6 & RC7
    ANSELCbits.ANSC6 = 0;
    ANSELCbits.ANSC7 = 0;
    // map TX1 to RC6
    RC6PPS = 0x0f;
    // RX1 maps to RC7 by default
    
    // Initialize the SPxBRGH, SPxBRGL register pair (p493)
    // 9600 BAUD (FOSC = 32 MHz)
    SP1BRGL = 51;
    SP1BRGH = 0;
    
    // and the BRGH and BRG16 bits (p493)
    TX1STAbits.BRGH = 0;
    BAUD1CONbits.BRG16 = 0;
    
    // Enable the asynchronous serial port by clearing 
    // the SYNC bit and setting the SPEN bit (p493)
    TX1STAbits.SYNC = 0;
    RC1STAbits.SPEN = 1;
    
        // If 9-bit transmission is desired, set the TX9 control bit. 
        // A set ninth data bit will indicate that the eight Least Significant 
        //data bits are an address when the receiver is set for address detection.
        
        // Set SCKP bit if inverted transmit is desired. 
    
    // Enable the transmission by setting the TXEN control 
    // bit. This will cause the TXxIF interrupt bit to be set.
    TX1STAbits.TXEN = 1;
    // Enable the reception by setting the CREN bit
    RC1STAbits.CREN = 1;
    
        // If interrupts are desired, set the TXxIE interrupt enable bit of 
        // the PIE3 register. An interrupt will occur immediately provided 
        // that the GIE and PEIE bits of the INTCON register are also set.
        
        // If 9-bit transmission is selected, the ninth bit should be loaded 
        // into the TX9D data bit.
    
    // Load 8-bit data into the TXxREG register. This will start the transmission.
    // DO THIS NOT IN INITIALIAZATION 
    // TX1REG
}

/****************************************************************************
 Function
  putch
 Parameters
  char the character to print
 Returns
     none.
 Description
  send a single character to EUSART1 (default to RC7)
 Notes
  This function, named in this way is how we connect printf, puts, etc. to
 the serial port on the PIC
 Author
     J. Edward Carryer, 04/20/19 14:20
 ****************************************************************************/
void putch(char data) {
  while( ! TX1IF)   // check if last character finished
  {}                // wait till done
  TX1REG = data;    // send data
}

/****************************************************************************
 Function
  getchar
 Parameters
  none
 Returns
  int The character received (use int return to match C99)  
 Description
  send a single character to EUSART1
 Notes
  The prototype for this is in stdio.h
 Author
  J. Edward Carryer, 04/20/19 14:43
 ****************************************************************************/
int getchar(void) {
  while( ! RC1IF)   // check buffer
  {}                // wait till ready
  return RC1REG;
}
