/*
 * File:   main.c
 * Author: jeremydahl
 *
 * Created on August 18, 2019, 3:05 PM
 */

#include "config.h"
#include <stdbool.h>
#include <xc.h>
#include <stdint.h>
//#include <pic16f15375.h>
#include "functions.h"
#include "IndividuallyAddressableLEDs.h"
#include "TM1637Display.h"
#include "ScorekeepingSM.h"

// Variables
char byte1 = 0b00011000;
char byte2 = 0b00011000;
char byte3 = 0b00011000;
char byte4 = 0b00011000;
char byte5 = 0b00011000;
char byte6 = 0b00011000;
char byte7 = 0b00011000;
char byte8 = 0b00011000;
char byte9 = 0b00011000;
char byte10 = 0b00011000;
char byte11 = 0b00011000;
char byte12 = 0b00011000;
char clearFifo = 0;

static char MarblePositions[13] = {0};

int TeamScores[4] = {0,0,0,0};
double digit = 0;
uint8_t counter = 0;
States Turn;
States Last;

static uint8_t MarbleStates[97] = //{0};
    {0,0,0,0,0,0,0,0,   0,0,1,1,1,1,1,1,    1,1,0,0,0,0,0,0,
     0,0,0,0,0,0,0,0,   0,0,2,2,2,2,2,2,    2,2,0,0,0,0,0,0,
     0,0,0,0,0,0,0,0,   0,0,3,3,3,3,3,3,    3,3,0,0,0,0,0,0,
     0,0,0,0,0,0,0,0,   0,0,4,4,4,4,4,4,    4,4,0,0,0,0,0,0,    0}; 

// Functions
void PIC1(void);
void PIC2(void);
void PIC3(void);
void PIC4(void);
void PIC5(void);

// Main
void main(void) {
// initialize   
    // Disable interrupts
    INTCONbits.GIE = 0; 
    INTCONbits.PEIE = 0;
    
    InitInputPorts();
    InitEUSART();
    

//loop forever...  
    // Return from interrupt
        // disable interrupt
        // Receive data
        // Process data
        // write LEDs
        // write scoreboard
        // Enable Interrupts
    
//    RC1STAbits.CREN = 0;    // clears receive overrun error
//    clearFifo = RC1REG;     // clears register
//    clearFifo = RC1REG;     // clears register in case of 2nd data... 
//    PIE3bits.RC1IE = 1;     // enable RX interrupt
//    INTCONbits.PEIE = 1;    // enable peripheral interrupts
//    INTCONbits.GIE = 1;     // enable global interrupts
    PIC5();
    return;  
}

void PIC1(void){
    while(1){
        __delay_ms(2000);   // wait .5 seconds
        writePortStates(); 
    }
}
void PIC2(void){
    byte1 = getchar();  // receive incoming data
    byte2 = getchar();
    byte3 = getchar();    
    putch(byte1);       // write previous PIC I/Os
    putch(byte2);
    putch(byte3);
    writePortStates();  // write pin states
    __delay_ms(40);
}
void PIC3(void){
    byte1 = getchar();  // receive incoming data
    byte2 = getchar();
    byte3 = getchar(); 
    byte4 = getchar(); 
    byte5 = getchar(); 
    byte6 = getchar(); 
    putch(byte1);       // write previous PIC I/Os
    putch(byte2);
    putch(byte3);
    putch(byte4);
    putch(byte5);
    putch(byte6);
    writePortStates();  // write pin states
    __delay_ms(40);
}
void PIC4(void){
    byte1 = getchar();  // receive incoming data
    byte2 = getchar();
    byte3 = getchar(); 
    byte4 = getchar(); 
    byte5 = getchar(); 
    byte6 = getchar(); 
    byte7 = getchar(); 
    byte8 = getchar(); 
    byte9 = getchar(); 
    putch(byte1);       // write previous PIC I/Os
    putch(byte2);
    putch(byte3);
    putch(byte4);
    putch(byte5);
    putch(byte6);
    putch(byte7);
    putch(byte8);
    putch(byte9);
    writePortStates();  // write pin states
    __delay_ms(40);
}
// Main PIC
void PIC5(void){
    Init_IA_LEDs();
    InitTM1637Display();
    setDisplayBrightness(0xFF, true);
    LATCbits.LATC4 = 0;     //Low
    TRISCbits.TRISC4 = 1;   //Input
    ANSELCbits.ANSC4 = 0;   //Digital I/O
    WPUCbits.WPUC4 = 1;     //Pull up enabled
    
    uint8_t Counter = 0;
    InitMarblePositions();
    
    // Set RB1 as digital input with pull up on
    LATBbits.LATB1 = 0;     //Low
    TRISBbits.TRISB1 = 1;   //Input
    ANSELBbits.ANSB1 = 0;   //Digital I/O
    WPUBbits.WPUB1 = 1;     //Pull up enabled
    
    if(PORTBbits.RB1 == 0){ // If button is pressed during reset, then 2 player mode
                while(1){
        // Receive incoming data
        for(int i=0; i<12; i++)
        {
            MarblePositions[i] = getchar();
        }
        MarblePositions[12] = PORTCbits.RC4;

        // Convert Marble Positions to Game States
        TeamMarbleSM(MarblePositions, MarbleStates);    
         //        MarblePositionsToStates(MarblePositions, MarbleStates, 1);

        // Write marble states
         //        writeMarblePositionstoLED(  ~MarblePositions[0], ~MarblePositions[1], 
        //                                    ~MarblePositions[2], ~MarblePositions[3],
        //                                    ~MarblePositions[4], ~MarblePositions[5], 
        //                                    ~MarblePositions[6], ~MarblePositions[7],
        //                                    ~MarblePositions[8], ~MarblePositions[9],
        //                                    ~MarblePositions[10], ~MarblePositions[11],
        //                                    ~PORTCbits.RC4);
        writeMarbleStatestoLED(MarbleStates);
        CalculateScores(TeamScores, MarbleStates);

        // Allow LEDs to reset
            __delay_ms(4);

        // Print scores
        DisplayScore(TeamScores[0], P1);
        DisplayScore(TeamScores[1], P2);
        DisplayScore(TeamScores[0], P3);
        DisplayScore(TeamScores[1], P4);

        digit++;
        if(digit/2 > 20){
            digit = -20;
        }
        }
    }else{      // Default to 1 player mode
        while(1){
        
        // Receive incoming data
        for(int i=0; i<12; i++)
        {
            MarblePositions[i] = getchar();
        }
        MarblePositions[12] = PORTCbits.RC4;

        // Convert Marble Positions to Game States
        MarbleSM(MarblePositions, MarbleStates);    

        // Write LEDS
        if( (counter%2) == 0){       
            //If digit is even
            Turn = GetCurrentState();
            Last = GetLastTurn();
            TurnIndicator(MarbleStates,Last,Turn);
            //function to turn off player turns LED
        }else{
            // normal lights
            writeMarbleStatestoLED(MarbleStates);
        }
        CalculateScores(TeamScores, MarbleStates);

        // Allow LEDs to reset
            __delay_ms(4);

        // Print scores
        DisplayScore(TeamScores[0], P1);
        DisplayScore(TeamScores[1], P2);
        DisplayScore(TeamScores[2], P3);
        DisplayScore(TeamScores[3], P4);

        digit++;
        counter++;
        if(digit/2 > 20){
            digit = -20;
        }
        }
    }
    
   
    return;
}