
//  Author: avishorp@gmail.com
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <stdbool.h>

#include "TM1637Display.h"
#include <xc.h>

#define TM1637_I2C_COMM1    0x40
#define TM1637_I2C_COMM2    0xC0
#define TM1637_I2C_COMM3    0x80

#define m_pinClk    0
#define m_pinDIO    1              
#define OUTPUT      true
#define INPUT       false


//
//      A
//     ---
//  F |   | B
//     -G-
//  E |   | C
//     ---
//      D
const uint8_t digitToSegment[] = {
 // XGFEDCBA
  0b00111111,    // 0
  0b00000110,    // 1
  0b01011011,    // 2
  0b01001111,    // 3
  0b01100110,    // 4
  0b01101101,    // 5
  0b01111101,    // 6
  0b00000111,    // 7
  0b01111111,    // 8
  0b01101111,    // 9
  0b01110111,    // A
  0b01111100,    // b
  0b00111001,    // C
  0b01011110,    // d
  0b01111001,    // E
  0b01110001     // F
  };


void InitTM1637Display(void)
{
    // Pins RA0, RA2, RA4, RA6 as CLK
    // Pins RA1, RA3, RA5, RA7 as DIO
    
    // Port A
    LATA =      0x00;   // start low
    TRISA =     0xFF;   // set port A as input
    ANSELA =    0x00;   // set port A to digital I/O
    WPUA =      0xFF;   // pull ups enabled
    
	// Set the pin direction and default value.
	// Both pins are set as inputs, allowing the pull-up resistors to pull them up
        //    pinMode(m_pinClk, INPUT, Player);
        //    pinMode(m_pinDIO,INPUT);
        //	digitalWrite(m_pinClk, LOW);
        //	digitalWrite(m_pinDIO, LOW);
}

void setDisplayBrightness(uint8_t brightness, bool on)
{
	m_brightness = (brightness & 0x7) | (on? 0x08 : 0x00);
}

void setDisplaySegments(const uint8_t segments[], uint8_t length, 
            uint8_t pos, ScoreBoard Player)
{
    // Write COMM1
	start(Player);
	writeDisplayByte(TM1637_I2C_COMM1, Player);
	stop(Player);

	// Write COMM2 + first digit address
	start(Player);
	writeDisplayByte(TM1637_I2C_COMM2 + (pos & 0x03), Player);

	// Write the data bytes
	for (uint8_t k=0; k < length; k++)
	  writeDisplayByte(segments[k], Player);

	stop(Player);

	// Write COMM3 + brightness
	start(Player);
	writeDisplayByte(TM1637_I2C_COMM3 + (m_brightness & 0x0f), Player);
	stop(Player);
}

void showDisplayNumberDec(int num, bool leading_zero, uint8_t length, uint8_t pos, ScoreBoard Player)
{
  showDisplayNumberDecEx(num, 0, leading_zero, length, pos, Player);
}

void showDisplayNumberDecEx(int num, uint8_t dots, bool leading_zero,
                                    uint8_t length, uint8_t pos, ScoreBoard Player)
{
  uint8_t digits[4];
	const static int divisors[] = { 1, 10, 100, 1000 };
	bool leading = true;

	for(int8_t k = 0; k < 4; k++) {
	    int divisor = divisors[4 - 1 - k];
		int d = num / divisor;
    uint8_t digit = 0;

		if (d == 0) {
		  if (leading_zero || !leading || (k == 3))
		      digit = encodeDisplayDigit(d);
	      else
		      digit = 0;
		}
		else {
			digit = encodeDisplayDigit(d);
			num -= d * divisor;
			leading = false;
		}
    
    // Add the decimal point/colon to the digit
    digit |= (dots & 0x80); 
    dots <<= 1;
    
    digits[k] = digit;
	}

	setDisplaySegments(digits + (4 - length), length, pos, Player);
}


static void bitDelay(void)
{
	//__delay_us(50);
        // 125 ns per operation. read-modify-write for bit operation. 50 us period..
    for(int i=0; i<22; i++){PORTBbits.RB0 = 0;};
    
}

static void start(ScoreBoard Player)
{
    pinMode(m_pinDIO, OUTPUT, Player);
    bitDelay();
}

static void stop(ScoreBoard Player)
{
    pinMode(m_pinDIO, OUTPUT, Player);
    bitDelay();
    pinMode(m_pinClk, INPUT, Player);    
	bitDelay();
    pinMode(m_pinDIO, INPUT, Player);
	bitDelay();
}

static bool writeDisplayByte(uint8_t b, ScoreBoard Player)
{
    uint8_t data = b;

    // 8 Data Bits
    for(uint8_t i = 0; i < 8; i++) {
        // CLK low
        pinMode(m_pinClk, OUTPUT, Player);
        bitDelay(); 

        // Set data bit
        if (data & 0x01){
            pinMode(m_pinDIO, INPUT, Player);
        }else{
            pinMode(m_pinDIO, OUTPUT, Player);
        }
        bitDelay();

        // CLK high
        pinMode(m_pinClk, INPUT, Player);
        bitDelay();
        
        data = data >> 1;
    }

    // Wait for acknowledge
    
    // CLK to zero
    pinMode(m_pinClk, OUTPUT, Player);
    pinMode(m_pinDIO, INPUT, Player);


    // CLK to high
    pinMode(m_pinClk, INPUT, Player);   
    bitDelay();
    
    uint8_t ack;
    switch(Player){
        case P1:
            ack = PORTAbits.RA1; //digitalRead(m_pinDIO);
            break;
        case P2:
            ack = PORTAbits.RA3; //digitalRead(m_pinDIO);
            break;
        case P3:
            ack = PORTAbits.RA5; //digitalRead(m_pinDIO);
            break;
        case P4:
            ack = PORTAbits.RA7; //digitalRead(m_pinDIO);
            break;
        default:
            break;
    }
    
    if (ack == 0){
        pinMode(m_pinDIO, OUTPUT, Player);
        bitDelay();
        
        pinMode(m_pinClk, OUTPUT, Player);
        bitDelay();

        return ack;
    }
}

uint8_t encodeDisplayDigit(uint8_t digit)
{
	return digitToSegment[digit & 0x0f];
}

void pinMode(uint8_t pin, bool mode, ScoreBoard Player){

    switch (Player) {
        case P1:
            if(pin == 1){
                if(mode == true){
                    // DIO as OUTPUT 
                    WPUAbits.WPUA1   = 0;   // Pull up resistor off
                    TRISAbits.TRISA1 = 0;   // DIO port as output
                    LATAbits.LATA1   = 0;   // DIO port low
                }else{
                    // DIO as INPUT
                    WPUAbits.WPUA1   = 1;   // Pull up resistor on
                    TRISAbits.TRISA1 = 1;   // DIO port as input
                }
            }else{ // pin == 0
                if(mode == true){
                    // CLK as OUTPUT 
                    WPUAbits.WPUA0   = 0;   // Pull up resistor off
                    TRISAbits.TRISA0 = 0;   // CLK port as output
                    LATAbits.LATA0   = 0;   // CLK port low
                }else{
                    // CLK as INPUT
                    WPUAbits.WPUA0   = 1;   // Pull up resistor on
                    TRISAbits.TRISA0 = 1;   // CLK port as input  
                }
            }
            break;
        case P2:
            if(pin == 1){
                if(mode == true){
                    // DIO as OUTPUT 
                    WPUAbits.WPUA3   = 0;   // Pull up resistor off
                    TRISAbits.TRISA3 = 0;   // DIO port as output
                    LATAbits.LATA3   = 0;   // DIO port low
                }else{
                    // DIO as INPUT
                    WPUAbits.WPUA3   = 1;   // Pull up resistor on
                    TRISAbits.TRISA3 = 1;   // DIO port as input
                }
            }else{ // pin == 0
                if(mode == true){
                    // CLK as OUTPUT 
                    WPUAbits.WPUA2   = 0;   // Pull up resistor off
                    TRISAbits.TRISA2 = 0;   // CLK port as output
                    LATAbits.LATA2   = 0;   // CLK port low
                }else{
                    // CLK as INPUT
                    WPUAbits.WPUA2   = 1;   // Pull up resistor on
                    TRISAbits.TRISA2 = 1;   // CLK port as input  
                }
            }
            break;
        case P3:
            if(pin == 1){
                if(mode == true){
                    // DIO as OUTPUT 
                    WPUAbits.WPUA5   = 0;   // Pull up resistor off
                    TRISAbits.TRISA5 = 0;   // DIO port as output
                    LATAbits.LATA5   = 0;   // DIO port low
                }else{
                    // DIO as INPUT
                    WPUAbits.WPUA5   = 1;   // Pull up resistor on
                    TRISAbits.TRISA5 = 1;   // DIO port as input
                }
            }else{ // pin == 0
                if(mode == true){
                    // CLK as OUTPUT 
                    WPUAbits.WPUA4   = 0;   // Pull up resistor off
                    TRISAbits.TRISA4 = 0;   // CLK port as output
                    LATAbits.LATA4   = 0;   // CLK port low
                }else{
                    // CLK as INPUT
                    WPUAbits.WPUA4   = 1;   // Pull up resistor on
                    TRISAbits.TRISA4 = 1;   // CLK port as input  
                }
            }
            break;
        case P4:
            if(pin == 1){
                if(mode == true){
                    // DIO as OUTPUT 
                    WPUAbits.WPUA7   = 0;   // Pull up resistor off
                    TRISAbits.TRISA7 = 0;   // DIO port as output
                    LATAbits.LATA7   = 0;   // DIO port low
                }else{
                    // DIO as INPUT
                    WPUAbits.WPUA7   = 1;   // Pull up resistor on
                    TRISAbits.TRISA7 = 1;   // DIO port as input
                }
            }else{ // pin == 0
                if(mode == true){
                    // CLK as OUTPUT 
                    WPUAbits.WPUA6   = 0;   // Pull up resistor off
                    TRISAbits.TRISA6 = 0;   // CLK port as output
                    LATAbits.LATA6   = 0;   // CLK port low
                }else{
                    // CLK as INPUT
                    WPUAbits.WPUA6   = 1;   // Pull up resistor on
                    TRISAbits.TRISA6 = 1;   // CLK port as input  
                }
            }
            break;   
        default:
            break;
    }
    return;
}

void DisplayLCD(int number, ScoreBoard Player){
// Number should be less than 10,000 to properly display
    uint8_t data[4];
    data[0] = encodeDisplayDigit(number/1000);
    data[1] = encodeDisplayDigit(number%1000/100);
    data[2] = encodeDisplayDigit(number%100/10);
    data[3] = encodeDisplayDigit(number%10);
    setDisplaySegments(data, 4, 0, Player);
    return;
}

void DisplayScore(int number, ScoreBoard Player){
    // Number between -9 and 99
    uint8_t data[4];
// Player
    data[0] = 0b01110011;   // "P"
    if(Player == P1){
        data[1] = 0b10000110;   // Team number + ":"   
    }else if(Player == P2)
    {
        data[1] = 0b11011011;   // Team number + ":"        
    }else if(Player == P3)
    {
        data[1] = 0b11001111;
    }else if(Player == P4)
    {
        data[1] = 0b11100110;
    }else{
        data[1] = 0x11;
    }

// Score    
    if(number<0){
        data[2] = 0b01000000;
        data[3] = encodeDisplayDigit(-1*number%10);
    }else if(number <10){
        data[2] = 0b00000000;
        data[3] = encodeDisplayDigit(number%10);
    }
    else{
        data[2] = encodeDisplayDigit(number%100/10);
        data[3] = encodeDisplayDigit(number%10);
    }
    setDisplaySegments(data, 4, 0, Player);
    return;
}