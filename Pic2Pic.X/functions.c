/*
 * File:   functions.c
 * Author: jeremydahl
 *
 * Created on September 4, 2019, 1:52 PM
 */

#include <stdbool.h>
#include <xc.h>
#include <stdint.h>
//#include <pic16f15375.h>
#include "config.h"

void write12BytesWait1Sec(void){
    putch('a');
    putch('b');
    putch('c'); // 3
    putch('d');
    putch('e');
    putch('f'); // 6
    putch('g');
    putch('h');
    putch('i'); // 9
    putch('j');
    putch('k');
    putch('l'); // 12
    __delay_ms(4000); // delays 1000 ms
    return;
}

void writePortAStates(void){
    putch(PORTA);
    return;
}

void writePortStates(void){
    putch(PORTA);
    putch(PORTB);
    putch(PORTD);
    return;
}

void concatenatePortStates(void){
    // if data received
    
    // save data
    char data1;
    data1 = getchar();
    
    // write out port states
    writePortAStates();
    
    // write saved data
    putch(data1);
    
    return;
}

