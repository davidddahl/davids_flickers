/*
 * State Machine for keeping track of scores in flickers
 * 
 * Input:
 *      marble states
 * 
 * Outputs:
 *      player scores
 * 
 * Author:
 *      Jeremy Dahl
 * 
 * Revisions:
 *      04.25.2020 16:10 - Initial Version
 */


#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>
#include <pic16f15356.h>

#include "ScorekeepingSM.h"

#define NUM_LEDS 97       // How many leds are in the strip?
#define BRIGHTNESS 100    // 0-255

#define TEAM1_START 1   // Home base numbers
#define TEAM1_END   2   //4
#define TEAM2_START 16
#define TEAM2_END   17  //19
#define TEAM3_START 31
#define TEAM3_END   34
#define TEAM4_START 46
#define TEAM4_END   49


// Constant Definitions
#define NUM_LEDS 97
#define TEAM1 1
#define TEAM2 2
#define TEAM3 3
#define TEAM4 4



// Variable definitions
static States CurrentState = Init;
static States NextState = Init;
static States LastTurn = Team4Turn;
static Events CurrentEvent = INIT;
static Events LastEvent = INIT;        

static uint8_t LastMarblePositions[13] = {0};
static uint8_t MarbleSettlingTime = 0;
static bool TurnComplete = false;
#define N_SettlingCycles 2
int r = 0;

//    {0, 9,18,22,    //         Team 1: 0-9,  18-22      1
//    24,33,42,46,    //         Team 2: 24-33,42-46      2
//    48,57,66,70,    //         Team 3: 48-57,66-70      3
//    72,81,90,94,    //         Team 4: 72-81,90-94      4
//    23,47,71,95,    //             +5: 23,47,71,95      5
//    10,17,          // Anti revival 1: 10-17            6
//    34,41,          // Anti revival 1: 34-41            7
//    58,65,          // Anti revival 1: 58-65            8
//    82,89,          // Anti revival 1: 82-89            9
//    96};            //            +10: 96               10

// Main function
void InitMarblePositions(void){
    LastMarblePositions[0] = 0b11111111;
    LastMarblePositions[1] = 0b00000011;
    LastMarblePositions[2] = 0b11111100;
    LastMarblePositions[3] = 0b11111111;
    LastMarblePositions[4] = 0b00000011;
    LastMarblePositions[5] = 0b11111100;
    LastMarblePositions[6] = 0b11111111;
    LastMarblePositions[7] = 0b00000011;
    LastMarblePositions[8] = 0b11111100;
    LastMarblePositions[9] = 0b11111111;
    LastMarblePositions[10] = 0b00000011;
    LastMarblePositions[11] = 0b11111100;
    LastMarblePositions[12] = 0b11111111;
    return;
}

void TestMarbleSM(uint8_t MarblePositions[], uint8_t MarbleStates[]){
    uint8_t Temp = 0;
    uint8_t Player = 0;
    for(int i=0; i<12; i++){
        Player = i/3+1;
        Temp = MarblePositions[i];
        for(int j=0; j<8; j++){
            if( ( (Temp>>j) & 0x01 ) == 0x01 ){
                MarbleStates[i*8+j] = 0;
            }else{
                MarbleStates[i*8+j] = Player;
            }
        }
    }
    return;
}

void MarbleSM(uint8_t MarblePositions[], uint8_t MarbleStates[]) {
// Check Marble Positions
    
// Run Gameplay SM
    switch(CurrentState){
        case Init:
            // Check if marbles initialized..
            if( compareArrays(MarblePositions, LastMarblePositions, 0, 12) ){
                // if yes 
                // change colors to team
                // state player 1 turn
                for(int i=10; i<18; i++){
                    MarbleStates[i] = 1;
                }
                for(int i=34; i<42; i++){
                    MarbleStates[i] = 2;
                }
                for(int i=58; i<66; i++){
                    MarbleStates[i] = 3;
                }
                for(int i=82; i<90; i++){
                    MarbleStates[i] = 4;
                }
                NextState = Wait4Marbles;
                LastTurn = Team4Turn;
            }else{
                // keep colors white
                // state init
                MarblePositionsToStates(MarblePositions,MarbleStates,7);
                NextState = Init;
            }
                
            break;
        case Team1Turn:
            // Check if marbles moved
            //
            for (r=0;r<=3;r++)
                LastMarblePositions;
            
            
            //
            if( compareArrays(MarblePositions, LastMarblePositions, 0, 12) == false ){
                MarblePositionsToStates(MarblePositions,MarbleStates,1);
                LastTurn = Team1Turn;
                NextState = Wait4Marbles;
                for(int i=0; i<13; i++){
                    LastMarblePositions[i] = MarblePositions[i];
                }
            }else{
                NextState = Team1Turn;
            }
            // if yes
                // switch to wait4marbles
                // change last turn state
            // else
                // state = current state
            break;
        case Team2Turn:
            if( compareArrays(MarblePositions, LastMarblePositions, 0, 12) == false ){
                MarblePositionsToStates(MarblePositions,MarbleStates,2);
                LastTurn = Team2Turn;
                NextState = Wait4Marbles;
                for(int i=0; i<13; i++){
                    LastMarblePositions[i] = MarblePositions[i];
                }
            }else{
                NextState = Team2Turn;
            }
            break;
        case Team3Turn:
            if( compareArrays(MarblePositions, LastMarblePositions, 0, 12) == false ){
                MarblePositionsToStates(MarblePositions,MarbleStates,3);
                LastTurn = Team3Turn;
                NextState = Wait4Marbles;
                for(int i=0; i<13; i++){
                    LastMarblePositions[i] = MarblePositions[i];
                }
            }else{
                NextState = Team3Turn;
            }
            break;
        case Team4Turn:
            if( compareArrays(MarblePositions, LastMarblePositions, 0, 12) == false ){
                MarblePositionsToStates(MarblePositions,MarbleStates,4);
                LastTurn = Team4Turn;
                NextState = Wait4Marbles;
                for(int i=0; i<13; i++){
                    LastMarblePositions[i] = MarblePositions[i];
                }
            }else{
                NextState = Team4Turn;
            }
            break;
        case FixBadFlick:
            break;
        case Wait4Marbles:
            if(PORTBbits.RB1 == 0){
                MarbleSettlingTime = 0;
                switch(LastTurn){
                    case Team1Turn:
                        NextState = Team2Turn;
                        break;
                    case Team2Turn:
                        NextState = Team3Turn;
                        break;
                    case Team3Turn:
                        NextState = Team4Turn;
                        break;
                    case Team4Turn:
                        NextState = Team1Turn;
                        break;                    
                }
            }else{
                NextState = Wait4Marbles;
                MarbleSettlingTime ++;
            }
            break;
        case EndGame:
            break;
        default:
            break;
    }

    CurrentState = NextState;
}

void TeamMarbleSM(uint8_t MarblePositions[], uint8_t MarbleStates[]) {
// Check Marble Positions
    
// Run Gameplay SM
    switch(CurrentState){
        case Init:
            // Check if marbles initialized..
            if( compareArrays(MarblePositions, LastMarblePositions, 0, 12) ){
                // if yes 
                // change colors to team
                // state team 1 turn
                for(int i=10; i<18; i++){
                    MarbleStates[i] = 5;
                }
                for(int i=34; i<42; i++){
                    MarbleStates[i] = 6;
                }
                for(int i=58; i<66; i++){
                    MarbleStates[i] = 5;
                }
                for(int i=82; i<90; i++){
                    MarbleStates[i] = 6;
                }
                NextState = Wait4Marbles;
                LastTurn = Team6Turn;
            }else{
                // keep colors white
                // state init
                MarblePositionsToStates(MarblePositions,MarbleStates,7);
                NextState = Init;
            }
                
            break;
        case Team5Turn:
            // Check if marbles moved
            if( compareArrays(MarblePositions, LastMarblePositions, 0, 12) == false ){
                MarblePositionsToStates(MarblePositions,MarbleStates,5);
                LastTurn = Team5Turn;
                NextState = Wait4Marbles;
                for(int i=0; i<13; i++){
                    LastMarblePositions[i] = MarblePositions[i];
                }
            }else{
                NextState = Team6Turn;
            }
            // if yes
                // switch to wait4marbles
                // change last turn state
            // else
                // state = current state
            break;
        case Team6Turn:
            if( compareArrays(MarblePositions, LastMarblePositions, 0, 12) == false ){
                MarblePositionsToStates(MarblePositions,MarbleStates,6);
                LastTurn = Team6Turn;
                NextState = Wait4Marbles;
                for(int i=0; i<13; i++){
                    LastMarblePositions[i] = MarblePositions[i];
                }
            }else{
                NextState = Team5Turn;
            }
            break;
        case FixBadFlick:
            break;
        case Wait4Marbles:
            if(PORTBbits.RB1 == 0){
                MarbleSettlingTime = 0;
                switch(LastTurn){
                    case Team5Turn:
                        NextState = Team6Turn;
                        break;
                    case Team6Turn:
                        NextState = Team5Turn;
                        break;                  
                }
            }else{
                NextState = Wait4Marbles;
                MarbleSettlingTime ++;
            }
            break;
        case EndGame:
            break;
        default:
            break;
    }

    CurrentState = NextState;
}


void UpdateMarbleStates(uint8_t MarblePositions[],uint8_t MarbleStates[]){
    uint8_t Player = 5;
    uint8_t TempMarblePos1 = 0;
    uint8_t TempMarblePos2 = 0;
    uint8_t TempLastMarblePos1 = 0;
    uint8_t TempLastMarblePos2 = 0;
    
    // determine last person to go
        TempMarblePos1 = MarblePositions[1];
        TempMarblePos2 = MarblePositions[2];
        TempLastMarblePos1 = LastMarblePositions[1];
        TempLastMarblePos2 = LastMarblePositions[2];
    if( ((TempMarblePos1 & 0b00111111) != (TempLastMarblePos1 & 0b00111111)) ||
        ((TempMarblePos2 & 0b11000000) != (TempLastMarblePos2 & 0b11000000)) ){
        Player = 1;
    }
        TempMarblePos1 = MarblePositions[4];
        TempMarblePos2 = MarblePositions[5];
        TempLastMarblePos1 = LastMarblePositions[4];
        TempLastMarblePos2 = LastMarblePositions[5];
    if( ((TempMarblePos1 & 0b00111111) != (TempLastMarblePos1 & 0b00111111)) ||
        ((TempMarblePos2 & 0b11000000) != (TempLastMarblePos2 & 0b11000000)) ){
        Player = 2;
    }
        TempMarblePos1 = MarblePositions[7];
        TempMarblePos2 = MarblePositions[8];
        TempLastMarblePos1 = LastMarblePositions[7];
        TempLastMarblePos2 = LastMarblePositions[8];
    if( ((TempMarblePos1 & 0b00111111) != (TempLastMarblePos1 & 0b00111111)) ||
        ((TempMarblePos2 & 0b11000000) != (TempLastMarblePos2 & 0b11000000)) ){
        Player = 3;
    }
        TempMarblePos1 = MarblePositions[10];
        TempMarblePos2 = MarblePositions[11];
        TempLastMarblePos1 = LastMarblePositions[10];
        TempLastMarblePos2 = LastMarblePositions[11];
    if( ((TempMarblePos1 & 0b00111111) != (TempLastMarblePos1 & 0b00111111)) ||
        ((TempMarblePos2 & 0b11000000) != (TempLastMarblePos2 & 0b11000000)) ){
        Player = 4;
    }
//    if( ((MarblePositions[4] & 0b00111111) != (LastMarblePositions[4] & 0b00111111)) ||
//        ((MarblePositions[5] & 0b11000000) != (LastMarblePositions[5] & 0b11000000)) ){
//        Player = 2;
//    }
//    if( ((MarblePositions[7] & 0b00111111) != (LastMarblePositions[7] & 0b00111111)) ||
//        ((MarblePositions[8] & 0b11000000) != (LastMarblePositions[8] & 0b11000000)) ){
//        Player = 3;
//    }
//    if( ((MarblePositions[10] & 0b00111111) != (LastMarblePositions[10] & 0b00111111)) ||
//        ((MarblePositions[11] & 0b11000000) != (LastMarblePositions[11] & 0b11000000)) ){
//        Player = 4;
//    }
    
    // update LastMarblePositions
    for(int i=0; i<13; i++){
        LastMarblePositions[i] = MarblePositions[i];
    }

    // update new MarblePositions
    TestMarbleSM(MarblePositions, MarbleStates);
//    for(int i=0; i<97; i++){
//        if( (MarbleStates[i] == 0) && ( ( (MarblePositions[i/8]>>(i%8)) & 0x01 ) == 1 ) ){
//            MarbleStates[i] = Player;
//        }
//        if( (MarbleStates[i] != 0) && ( ( (MarblePositions[i/8]>>(i%8)) & 0x01 ) == 0 ) ){
//            MarbleStates[i] = 0;
//        }
//         compare to current marble positions
//             if empty then turn off
//             if full then set to team
//    }
    
    return;
}

bool compareArrays(uint8_t a[], uint8_t b[], uint8_t n, uint8_t m) {
  uint8_t ii;
  for(ii = n; ii < m; ii++) {
    if (a[ii] != b[ii]) return false;
    // better:
    // if(fabs(a[ii]-b[ii]) < 1e-10 * (fabs(a[ii]) + fabs(b[ii]))) {
    // with the appropriate tolerance
  }
  return true;
}
//davids work
void MarblePositionsToStates(uint8_t MarblePositions[], uint8_t MarbleStates[], uint8_t Player){
    // compare each marble position to marble state
    // if empty then full give to Player
    // if full then empty, clear
    
    for(int i=0; i<97; i++){
        
        // if marble position is full
        if( (( (0x01<<(i%8)) & MarblePositions[i/8] ) == 0) ){
            if(MarbleStates[i] == 0){
                // and the hole was previously empty, give it to current player
                MarbleStates[i] = Player;
            }else if(Player > 6){
                MarbleStates[i] = Player;
            }
            // otherwise do nothing  
        }else{
            // hole is empty
            MarbleStates[i] = 0;
        }
    }
    return;
}

void CalculateScores(int TeamScores[], uint8_t MarbleStates[]){
    for( int i=0; i<4; i++ ){
        TeamScores[i] = 0;
    }
    
    for( int i=0; i<97; i++ ){
        if( MarbleStates[i] == 1 ){
            if( (i <  10) )                 // P1 Territory
                {TeamScores[0] = TeamScores[0] - 1;}               
            if( (i >= 10) && (i < 18) )     // P1 Home 
                {}
            if( (i >= 18) && (i < 23) )     // P1 Territory
                {TeamScores[0] = TeamScores[0] - 1;}
            
            if( (i >= 24) && (i < 34) )     // P2 Territory
                {TeamScores[0]++;}
            if( (i >= 34) && (i < 42) )     // P2 Home
                {}
            if( (i >= 42) && (i < 47) )     // P2 Territory
                {TeamScores[0]++;}
            
            if( (i >= 48) && (i < 58) )   // P3 Territory
                {TeamScores[0]++;}
            if( (i >= 58) && (i < 66) )   // P3 Home
                {}
            if( (i >= 66) && (i < 71) )   // P3 Territory
                {TeamScores[0]++;}
            
            if( (i >= 72) && (i < 82) )   // P4 Territory
                {TeamScores[0]++;}
            if( (i >= 82) && (i < 90) )   // P4 Home
                {}
            if( (i >= 90) && (i < 95) )   // P4 Territory
                {TeamScores[0]++;}
            
            if( (i == 23) || (i == 47) || (i == 71) || (i == 95) )   // +5
                {TeamScores[0] = TeamScores[0]+5;}
            if( i == 96 )   // +10
                {TeamScores[0] = TeamScores[0]+10;}
        }
        if( MarbleStates[i] == 2 ){
            if( (i <  10) )                 // P1 Territory
                {TeamScores[1]++;}
            if( (i >= 10) && (i < 18) )     // P1 Home 
                {}
            if( (i >= 18) && (i < 23) )     // P1 Territory
                {TeamScores[1]++;}
            
            if( (i >= 24) && (i < 34) )     // P2 Territory
                {TeamScores[1] = TeamScores[1] - 1;}
            if( (i >= 34) && (i < 42) )     // P2 Home
                {}
            if( (i >= 42) && (i < 47) )     // P2 Territory
                {TeamScores[1] = TeamScores[1] - 1;}
            
            if( (i >= 48) && (i < 58) )   // P3 Territory
                {TeamScores[1]++;}
            if( (i >= 58) && (i < 66) )   // P3 Home
                {}
            if( (i >= 66) && (i < 71) )   // P3 Territory
                {TeamScores[1]++;}
            
            if( (i >= 72) && (i < 82) )   // P4 Territory
                {TeamScores[1]++;}
            if( (i >= 82) && (i < 90) )   // P4 Home
                {}
            if( (i >= 90) && (i < 95) )   // P4 Territory
                {TeamScores[1]++;}
            
            if( (i == 23) || (i == 47) || (i == 71) || (i == 95) )   // +5
                {TeamScores[1] = TeamScores[1]+5;}
            if( i == 96 )   // +10
                {TeamScores[1] = TeamScores[1]+10;}
        }
        if( MarbleStates[i] == 3 ){
            if( (i <  10) )                 // P1 Territory
                {TeamScores[2]++;}              
            if( (i >= 10) && (i < 18) )     // P1 Home 
                {}
            if( (i >= 18) && (i < 23) )     // P1 Territory
                {TeamScores[2]++;}
            
            if( (i >= 24) && (i < 34) )     // P2 Territory
                {TeamScores[2]++;}
            if( (i >= 34) && (i < 42) )     // P2 Home
                {}
            if( (i >= 42) && (i < 47) )     // P2 Territory
                {TeamScores[2]++;}
            
            if( (i >= 48) && (i < 58) )   // P3 Territory
                {TeamScores[2] = TeamScores[2] - 1;}
            if( (i >= 58) && (i < 66) )   // P3 Home
                {}
            if( (i >= 66) && (i < 71) )   // P3 Territory
                {TeamScores[2] = TeamScores[2] - 1;}
            
            if( (i >= 72) && (i < 82) )   // P4 Territory
                {TeamScores[2]++;}
            if( (i >= 82) && (i < 90) )   // P4 Home
                {}
            if( (i >= 90) && (i < 95) )   // P4 Territory
                {TeamScores[2]++;}
            
            if( (i == 23) || (i == 47) || (i == 71) || (i == 95) )   // +5
                {TeamScores[2] = TeamScores[2]+5;}
            if( i == 96 )   // +10
                {TeamScores[2] = TeamScores[2]+10;}
        }
        if( MarbleStates[i] == 4 ){
            if( (i <  10) )                 // P1 Territory
                {TeamScores[3]++;}               
            if( (i >= 10) && (i < 18) )     // P1 Home 
                {}
            if( (i >= 18) && (i < 23) )     // P1 Territory
                {TeamScores[3]++;}
            
            if( (i >= 24) && (i < 34) )     // P2 Territory
                {TeamScores[3]++;}
            if( (i >= 34) && (i < 42) )     // P2 Home
                {}
            if( (i >= 42) && (i < 47) )     // P2 Territory
                {TeamScores[3]++;}
            
            if( (i >= 48) && (i < 58) )   // P3 Territory
                {TeamScores[3]++;}
            if( (i >= 58) && (i < 66) )   // P3 Home
                {}
            if( (i >= 66) && (i < 71) )   // P3 Territory
                {TeamScores[3]++;}
            
            if( (i >= 72) && (i < 82) )   // P4 Territory
                {TeamScores[3] = TeamScores[3] - 1;}
            if( (i >= 82) && (i < 90) )   // P4 Home
                {}
            if( (i >= 90) && (i < 95) )   // P4 Territory
                {TeamScores[3] = TeamScores[3] - 1;}
            
            if( (i == 23) || (i == 47) || (i == 71) || (i == 95) )   // +5
                {TeamScores[3] = TeamScores[3]+5;}
            if( i == 96 )   // +10
                {TeamScores[3] = TeamScores[3]+10;}
        }
        
        /***************************** Team Mode Scoring **************************************/
        if( MarbleStates[i] == 5 ){
            if( (i <  10) )                 // P1 Territory
                {TeamScores[0] = TeamScores[0] - 1;}
            if( (i >= 10) && (i < 18) )     // P1 Home 
                {}
            if( (i >= 18) && (i < 23) )     // P1 Territory
                {TeamScores[0] = TeamScores[0] - 1;}
            
            if( (i >= 24) && (i < 34) )     // P2 Territory
                {TeamScores[0]++;}
            if( (i >= 34) && (i < 42) )     // P2 Home
                {}
            if( (i >= 42) && (i < 47) )     // P2 Territory
                {TeamScores[0]++;}
            
            if( (i >= 48) && (i < 58) )   // P3 Territory
                {TeamScores[0] = TeamScores[0] - 1;}
            if( (i >= 58) && (i < 66) )   // P3 Home
                {}
            if( (i >= 66) && (i < 71) )   // P3 Territory
                {TeamScores[0] = TeamScores[0] - 1;}
            
            if( (i >= 72) && (i < 82) )   // P4 Territory
                {TeamScores[0]++;}
            if( (i >= 82) && (i < 90) )   // P4 Home
                {}
            if( (i >= 90) && (i < 95) )   // P4 Territory
                {TeamScores[0]++;}
            
            if( (i == 23) || (i == 47) || (i == 71) || (i == 95) )   // +5
                {TeamScores[0] = TeamScores[0]+5;}
            if( i == 96 )   // +10
                {TeamScores[0] = TeamScores[0]+10;}
        }
        if( MarbleStates[i] == 6 ){
            if( (i <  10) )                 // P1 Territory
                {TeamScores[1]++;}               
            if( (i >= 10) && (i < 18) )     // P1 Home 
                {}
            if( (i >= 18) && (i < 23) )     // P1 Territory
                {TeamScores[1]++;}
            
            if( (i >= 24) && (i < 34) )     // P2 Territory
                {TeamScores[1] = TeamScores[1] - 1;}
            if( (i >= 34) && (i < 42) )     // P2 Home
                {}
            if( (i >= 42) && (i < 47) )     // P2 Territory
                {TeamScores[1] = TeamScores[1] - 1;}
            
            if( (i >= 48) && (i < 58) )   // P3 Territory
                {TeamScores[1]++;}
            if( (i >= 58) && (i < 66) )   // P3 Home
                {}
            if( (i >= 66) && (i < 71) )   // P3 Territory
                {TeamScores[1]++;}
            
            if( (i >= 72) && (i < 82) )   // P4 Territory
                {TeamScores[1] = TeamScores[1] - 1;}
            if( (i >= 82) && (i < 90) )   // P4 Home
                {}
            if( (i >= 90) && (i < 95) )   // P4 Territory
                {TeamScores[1] = TeamScores[1] - 1;}
            
            if( (i == 23) || (i == 47) || (i == 71) || (i == 95) )   // +5
                {TeamScores[1] = TeamScores[1]+5;}
            if( i == 96 )   // +10
                {TeamScores[1] = TeamScores[1]+10;}
        }
    }
}

States GetCurrentState(void){
    return CurrentState;
    
}

States GetLastTurn(void){
    return LastTurn;
    
}
// Initialize flickers board array with intended marble positions
        // check if marbles are in proper positions
        // when in proper position begin game
    
    // BAD_FLICK
        // if marble leaves wrong hole
    
    // GOOD_FLICK
        // if marble leaves good hole
    
    // MARBLES_RETURNED
        // if currentPos = lastPos
    
    // MARBLES_HOME
        // if currentPos = initPos
    
    // UpdateScores
        // check which marbles are different from last time
        // if only the current team is different
            // add corresponding point value to that team
        // else if multiple teams are different
            // add highest point to current team
            // add lowest point to other team
        // endif
        // return updated score
    
    // UpdateLEDs
        // get potentiometer values and save them to a temporary variable
        // loop through position array
            // set led color based on current marble position
            // if marble matches number then set that LED
            // else turn hole LED off
            // end loop
        // refresh the LEDs
    
    // ResetGame
        // set socres to zero
        // illuminate home LEDs
