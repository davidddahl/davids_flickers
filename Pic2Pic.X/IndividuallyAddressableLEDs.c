/* 
 * File:   IndividuallyAddressableLEDs.h
 * Author: jeremydahl
 *
 * Description:
 *      Self-build library to used individually addressable
 *      LEDs with PIC microprocessors. 
 * 
 * Created on March 21, 2020, 9:07 PM
 */

#include <xc.h>
#include <stdint.h>
//#include <pic16f15375.h>
#include "IndividuallyAddressableLEDs.h"
#include "Bitdefs.h"
#include "ScorekeepingSM.h"

void Init_IA_LEDs(void){
    // Initialize RC5 as data bit
    LATCbits.LATC5 =      0;    // start low
    TRISCbits.TRISC5 =     0;   // set as output
    ANSELCbits.ANSC5 =    0;    // set to digital I/O
        
    // Ensure all LEDs are off
    for(int i=0; i<100; i++){
        for(int j=0; j<3; j++){
            for(int k=0; k<8; k++){
                writeWS2811Bit(0);
            }
        }
    }
    return;
}

void writeWS2811Bit(uint8_t Bit){
    // write Low bit
    if(Bit == 0){
        // assume 125 ns per write operation.. (TBD)
        LATCbits.LATC5 = 1;
        LATCbits.LATC5 = 1;
        LATCbits.LATC5 = 1;
        LATCbits.LATC5 = 0;
        LATCbits.LATC5 = 0;
        LATCbits.LATC5 = 0;
        LATCbits.LATC5 = 0;
        LATCbits.LATC5 = 0;
        LATCbits.LATC5 = 0;
        LATCbits.LATC5 = 0;
        return;
    }else if(Bit == 1){
        LATCbits.LATC5 = 1;
        LATCbits.LATC5 = 1;
        LATCbits.LATC5 = 1;
        LATCbits.LATC5 = 1;
        LATCbits.LATC5 = 1;
        LATCbits.LATC5 = 0;
        LATCbits.LATC5 = 0;
        LATCbits.LATC5 = 0;
        LATCbits.LATC5 = 0;
        LATCbits.LATC5 = 0;
        return;
    }else{
        return;
    }
}

void writeWS2811Byte(uint8_t Byte){
    writeWS2811Bit( ( Byte & BIT0HI) == BIT0HI );
    writeWS2811Bit( ( ( Byte >> 1) & BIT0HI) == BIT0HI );
    writeWS2811Bit( ( ( Byte >> 2) & BIT0HI) == BIT0HI );
    writeWS2811Bit( ( ( Byte >> 3) & BIT0HI) == BIT0HI );
    writeWS2811Bit( ( ( Byte >> 4) & BIT0HI) == BIT0HI );
    writeWS2811Bit( ( ( Byte >> 5) & BIT0HI) == BIT0HI );
    writeWS2811Bit( ( ( Byte >> 6) & BIT0HI) == BIT0HI );
    writeWS2811Bit( ( ( Byte >> 7) & BIT0HI) == BIT0HI );
    return;
}

void writeWS2811Packet(uint8_t LED_R, uint8_t LED_G, uint8_t LED_B){
    writeWS2811Byte(LED_R);   // RED
    writeWS2811Byte(LED_G);     // GREEN
    writeWS2811Byte(LED_B);     // BLUE
    
//    /* Data order: R7-R1, G7-G1, B7-B1*/
//    // RED
//    writeWS2811Bit(1);
//    writeWS2811Bit(1);
//    writeWS2811Bit(1);
//    writeWS2811Bit(1);
//    writeWS2811Bit(1);
//    writeWS2811Bit(1);
//    writeWS2811Bit(1);
//    writeWS2811Bit(1);
//    
//    // GREEN
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    
//    // BLUE
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
//    writeWS2811Bit(0);
}

void numLEDTest(uint8_t NumLEDs){
    for(int i=0; i<NumLEDs; i++){
        writeWS2811Packet(255, 0, 0);
        writeWS2811Packet(0, 255, 0);
        writeWS2811Packet(0, 0, 255);
        writeWS2811Packet(255, 255, 0);
    }
    return;
}

void writeBytetoLED(uint8_t Byte){
    for( int i=0; i<8; i++ ){
        if( ( ( Byte >> i) & BIT0HI) == BIT0HI ){
            writeWS2811Packet(255, 255, 255);
        }else{
            writeWS2811Packet(0, 0, 0);
        }
    }
    return;
}

void writeColortoLED(uint8_t Color){
    switch(Color){
        case 0:
            writeWS2811Packet(0, 0, 0);
            break;
        case 1:
            writeWS2811Packet(0, 0, 255);
            break;
        case 2:
            writeWS2811Packet(255, 0, 0);
            break;
        case 3:
            writeWS2811Packet(0, 255, 0);
            break;
        case 4:
            writeWS2811Packet(255, 255, 0);
            break;
        case 5:
            writeWS2811Packet(255, 0, 255);
            break;
        case 6:
            writeWS2811Packet(0, 1, 1);
            break;
        default:
            writeWS2811Packet(255, 255, 255);
            break;
    }
    return;
}

void writeMarblePositionstoLED(uint8_t Byte1, uint8_t Byte2, uint8_t Byte3, 
                            uint8_t Byte4, uint8_t Byte5, uint8_t Byte6, 
                            uint8_t Byte7, uint8_t Byte8, uint8_t Byte9, 
                            uint8_t Byte10,uint8_t Byte11,uint8_t Byte12,
                            uint8_t CenterMarble){
    writeBytetoLED(Byte1);
    writeBytetoLED(Byte2);
    writeBytetoLED(Byte3);
    writeBytetoLED(Byte4);
    writeBytetoLED(Byte5);
    writeBytetoLED(Byte6);
    writeBytetoLED(Byte7);
    writeBytetoLED(Byte8);
    writeBytetoLED(Byte9);
    writeBytetoLED(Byte10);
    writeBytetoLED(Byte11);
    writeBytetoLED(Byte12);
    writeBytetoLED(CenterMarble);
    return;
}

void writeMarbleStatestoLED(uint8_t MarbleStates[]){
    for(int i=0; i<97; i++){
        writeColortoLED(MarbleStates[i]);
    }
    return;
}

void TurnIndicator(uint8_t MarbleStates[], States Last, States Current){
    if(Current == Wait4Marbles)
        switch (Last){
            case Team4Turn:   
                for(int i=0; i<97; i++){            // For player 1(last turn player 4)
                    if( ((i >= 10) && (i < 18)) ||
                        ((i >= 34) && (i < 42)) || 
                        ((i >= 58) && (i < 66)) || 
                        ((i >= 82) && (i < 90)) )     // All home bases
                    {
                        if (MarbleStates[i] == 1)
                            writeColortoLED(0);
                        else{
                            writeColortoLED(MarbleStates[i]);
                        }
                    }else{
                        writeColortoLED(MarbleStates[i]);
                    }
                }
                break;
            case Team1Turn:
                for(int i=0; i<97; i++){            // For player 2(last turn player 1)
                    if( ((i >= 10) && (i < 18)) ||
                        ((i >= 34) && (i < 42)) || 
                        ((i >= 58) && (i < 66)) || 
                        ((i >= 82) && (i < 90)) )     // All home bases
                    {
                        if (MarbleStates[i] == 2)
                            writeColortoLED(0);
                        else{
                            writeColortoLED(MarbleStates[i]);
                        }
                    }else{
                        writeColortoLED(MarbleStates[i]);  
                    }
                }
                break;
            case Team2Turn:
                for(int i=0; i<97; i++){            // For player 3(last turn player 2)
                    if( ((i >= 10) && (i < 18)) ||
                        ((i >= 34) && (i < 42)) || 
                        ((i >= 58) && (i < 66)) || 
                        ((i >= 82) && (i < 90)) )     // All home bases
                    {
                        if (MarbleStates[i] == 3)
                            writeColortoLED(0);
                        else{
                            writeColortoLED(MarbleStates[i]);
                        }
                    }else{
                        writeColortoLED(MarbleStates[i]);  
                    }
                }
                break;
            case Team3Turn:
                for(int i=0; i<97; i++){            // For player 4(last turn player 3)
                    if( ((i >= 10) && (i < 18)) ||
                        ((i >= 34) && (i < 42)) || 
                        ((i >= 58) && (i < 66)) || 
                        ((i >= 82) && (i < 90)) )     // All home bases
                    {
                        if (MarbleStates[i] == 4)
                            writeColortoLED(0);
                        else{
                            writeColortoLED(MarbleStates[i]);
                        }
                    }else{
                        writeColortoLED(MarbleStates[i]);  
                    }
                }
                break;
            default:
                break;
        }


    
    return;
}
