/* 
 * File:   ScorekeepingSM.h
 * Author: jeremydahl
 *
 * Created on April 25, 2020, 4:08 PM
 */

#ifndef SCOREKEEPINGSM_H
#define	SCOREKEEPINGSM_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdbool.h>

// Type definitions
typedef enum {
    Init, Team1Turn, Team2Turn, Team3Turn, Team4Turn,
            Team5Turn, Team6Turn,
    FixBadFlick, Wait4Marbles, EndGame
} States; 

typedef enum {
    GOOD_FLICK, TIMEOUT, BAD_FLICK, END_SIMULATOR, DISP_SCORE,
    INIT, MARBLES_RETURNED, POS_1, POS_5, POS_10, NEG_1
} Events;    
    
void InitMarblePositions(void);
void TestMarbleSM(uint8_t MarblePositions[], uint8_t MarbleStates[]);
void MarbleSM(uint8_t MarblePositions[], uint8_t MarbleStates[]);
void TeamMarbleSM(uint8_t MarblePositions[], uint8_t MarbleStates[]);
void UpdateMarbleStates(uint8_t MarblePositions[],uint8_t MarbleStates[]);
bool compareArrays(uint8_t a[], uint8_t b[], uint8_t n, uint8_t m);
void MarblePositionsToStates(uint8_t MarblePositions[], uint8_t MarbleStates[], uint8_t Player);
void CalculateScores(int TeamScores[], uint8_t MarblePositions[]);
States GetCurrentState(void);
States GetLastTurn(void);

#ifdef	__cplusplus
}
#endif

#endif	/* SCOREKEEPINGSM_H */

