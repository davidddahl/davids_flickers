/* 
 * File:   functions.h
 * Author: jeremydahl
 *
 * Created on March 21, 2020, 10:40 AM
 */

#ifndef FUNCTIONS_H
#define	FUNCTIONS_H

#ifdef	__cplusplus
extern "C" {
#endif

// functions here
void write12BytesWait1Sec(void);
void writePortAStates(void);
void writePortStates(void);
void concatenatePortStates(void);

#ifdef	__cplusplus
}
#endif

#endif	/* FUNCTIONS_H */

