/* 
 * File:   IndividuallyAddressableLEDs.h
 * Author: jeremydahl
 *
 * Created on March 21, 2020, 9:07 PM
 */

#ifndef INDIVIDUALLYADDRESSABLELEDS_H
#define	INDIVIDUALLYADDRESSABLELEDS_H

#include "ScorekeepingSM.h"

#ifdef	__cplusplus
extern "C" {
#endif
    
void Init_IA_LEDs(void);
void writeWS2811Bit(uint8_t Bit);
void writeWS2811Byte(uint8_t Byte);
void writeWS2811Packet(uint8_t LED_R, uint8_t LED_G, uint8_t LED_B);
void numLEDTest(uint8_t NumLEDs);
void writeBytetoLED(uint8_t Byte);
void writeColortoLED(uint8_t Color);
void writeMarblePositionstoLED(uint8_t Byte1, uint8_t Byte2, uint8_t Byte3, 
                            uint8_t Byte4, uint8_t Byte5, uint8_t Byte6, 
                            uint8_t Byte7, uint8_t Byte8, uint8_t Byte9, 
                            uint8_t Byte10,uint8_t Byte11,uint8_t Byte12,
                            uint8_t CenterMarble);
void writeMarbleStatestoLED(uint8_t MarbleStates[]);
void TurnIndicator(uint8_t MarbleStates[],States Last, States Current);

#ifdef	__cplusplus
}
#endif

#endif	/* INDIVIDUALLYADDRESSABLELEDS_H */

